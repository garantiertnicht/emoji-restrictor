name := "Emoji Restrictor"

version := "0.1"

scalaVersion := "2.13.10"

resolvers += Resolver.mavenCentral
resolvers += "m2-dv8tion" at "https://m2.dv8tion.net/releases"

libraryDependencies ++= Seq(
  "net.dv8tion" % "JDA" % "4.4.0_352"
)
