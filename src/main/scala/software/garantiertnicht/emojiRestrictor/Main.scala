/*
 * Emoji Restrictor limits emoji to roles
 * Copyright (C) 2019 garantiertnicht
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package software.garantiertnicht.emojiRestrictor

import net.dv8tion.jda.api.requests.GatewayIntent._
import net.dv8tion.jda.api.utils.MemberCachePolicy
import net.dv8tion.jda.api.utils.cache.CacheFlag._
import net.dv8tion.jda.api.{AccountType, JDABuilder}

object Main {
  def main(args: Array[String]): Unit = {
    val token = args.head
    JDABuilder
      .create(token, GUILD_MESSAGES, GUILD_EMOJIS, GUILD_MEMBERS)
      .addEventListeners(new Bot)
      .disableCache(CLIENT_STATUS, ACTIVITY, VOICE_STATE)
      .setMemberCachePolicy(MemberCachePolicy.NONE)
      .build()
  }
}
