/*
 * Emoji Restrictor limits emoji to roles
 * Copyright (C) 2019 garantiertnicht
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package software.garantiertnicht.emojiRestrictor

import java.io.{PrintWriter, StringWriter}
import java.security.MessageDigest
import java.time.Instant
import java.util
import java.util.Date
import net.dv8tion.jda.api.JDA.Status
import net.dv8tion.jda.api.entities._
import net.dv8tion.jda.api.{
  EmbedBuilder,
  JDA,
  MessageBuilder,
  OnlineStatus,
  Permission
}
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.Permission._
import net.dv8tion.jda.api.entities.MessageActivity.ActivityType
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleAddEvent
import net.dv8tion.jda.api.utils.AttachmentOption

import scala.util.Random
import scala.collection.JavaConverters._
import scala.collection.mutable

case class ManPage(
  aliases: Seq[String],
  overview: String,
  syntax: Seq[String],
  description: String,
  parameters: Map[String, String],
  examples: Seq[String],
  botPermissions: Seq[Permission],
  userPermissions: Seq[Permission],
  commandHandler: (TextChannel, Guild, Member, Array[String]) => Unit
) {
  def canonicalName =
    aliases.headOption
      .getOrElse(
        "[No canonical name — This should never happen. <https://blame.garantiertnicht.software/>]"
      )
}

class Bot extends ListenerAdapter {
  var evaluatedUsers = Set[Long]()
  var membersWhoKnowBetter = Map[(Long, Long), Long]()

  val addRoleCommand = ManPage(
    Seq("grant", "add", "allow", "a", "g", "+"),
    "Allows a role not already added to use an emoji",
    Seq("add <emoji…> <roles…>"),
    """Adds a role the list of roles to use an emoji. Emojis and roles can be in
      |any order. If called initialy, the emoji will no longer be usable by
      |everyone. Note that only users having such a role can use the emoji after
      |that, neither heigher-roles nor admins or even owners are excempt from
      |this unless added.
    """.stripMargin.linesIterator.mkString(" "),
    Map(
      (
        "emoji",
        "Any emoji, the ID of an emoji or the name of an emoji in colons"
      ),
      (
        "role",
        "Any role mention, the ID of a role or the name of a role in backticks"
      )
    ),
    Seq(
      "add {emoteMention} {roleMention}",
      "add {roleMention} {emoteMention}",
      "a :{emoteName}: `{roleName}`",
      "+ {emoteId} {roleId}"
    ),
    Seq(MESSAGE_READ, MESSAGE_WRITE, MANAGE_EMOTES, MESSAGE_EMBED_LINKS),
    Seq(MANAGE_EMOTES),
    addRoleCommandHandler
  )

  val removeRoleCommand = ManPage(
    Seq("revoke", "rem", "r", "delete", "del", "d", "remove", "-"),
    "Revokes an previously added the right to use an emoji",
    Seq("remove <emoji…> <roles…>"),
    """Removes a role from the list that can use the emoji. Emojis and
      |roles can be in any order. If no roles remain, everyone will be able to
      |use the emoji again.
    """.stripMargin.linesIterator.mkString(" "),
    Map(
      (
        "emoji",
        "Any emoji, the ID of an emoji or the name of an emoji in colons"
      ),
      (
        "role",
        "Any role mention, the ID of a role or the name of a role in backticks"
      )
    ),
    Seq(
      "add {emoteMention} {roleMention}",
      "add {roleMention} {emoteMention}",
      "a :{emoteName}: `{roleName}`",
      "+ {emoteId} {roleId}"
    ),
    Seq(MESSAGE_READ, MESSAGE_WRITE, MANAGE_EMOTES, MESSAGE_EMBED_LINKS),
    Seq(MANAGE_EMOTES),
    removeRoleCommandHandler
  )

  val helpCommand = ManPage(
    Seq("help", "h", "about", "ab", "invite", "i", "?"),
    "Get more information about any command",
    Seq("help [command]"),
    """It is quite hard for some space alien to keep alive.
      |By using help you make sure that endangered space aliens about to starve
      |get something that remotely resembles food.
    """.stripMargin.linesIterator.mkString(" "),
    Map(("command", "Get the man-page of a specific command.")),
    Seq("help", "help help", "help add", "help ?"),
    Seq(MESSAGE_READ, MESSAGE_WRITE, MESSAGE_EMBED_LINKS),
    Seq(),
    helpCommandHandler
  )

  val getRolesCommand = ManPage(
    Seq(
      "listroles",
      "list",
      "l",
      "queryroles",
      "query",
      "q",
      "getroles",
      "get",
      "g",
      "!"
    ),
    "Check the roles of an emote",
    Seq("listroles <emote>"),
    "Outputs an embed that shows all roles allowed to use a specified emote.",
    Map(("emote", "The emote to query")),
    Seq("listroles :{emoteName}:", "list {emoteId}", "query {emoteMention}"),
    Seq(MESSAGE_READ, MESSAGE_WRITE, MESSAGE_EMBED_LINKS),
    Seq(),
    getRolesCommandHandler
  )

  val evalCommand = ManPage(
    Seq("eval"),
    "This command is intentionally left undocumented.",
    Seq("eval …"),
    "This command is intentionally left undocumented.",
    Map(("…", "Arguments")),
    Seq("eval"),
    Seq(MESSAGE_READ, MESSAGE_WRITE, MESSAGE_EMBED_LINKS),
    Seq(),
    evaluateUserCommandHandler
  )

  val iKnowWhatIAmDoingCommand = ManPage(
    Seq("iknowwhatiamdoing"),
    "Sets the switch to `MORE_MAGIC`.",
    Seq("iknowwhatiamdoing"),
    "After executing this command, you have five minutes to perform any action " +
      "without the bot checking if it has too many permission. This time frame " +
      "gets extended every time you execute a command. " +
      "This also allows for the generation of exception dumps.",
    Map(),
    Seq("iknowwhatiamdoing"),
    Seq(MESSAGE_READ, MESSAGE_WRITE),
    Seq(),
    iKnowWhatIAmDoingCommandHandler
  )

  val commands: Seq[ManPage] = Seq(
    helpCommand,
    addRoleCommand,
    removeRoleCommand,
    getRolesCommand,
    evalCommand,
    iKnowWhatIAmDoingCommand
  )

  override def onMessageReceived(event: MessageReceivedEvent): Unit = {
    if (event.getMember == null || event.getMember.getUser.isBot) {
      return
    }

    val message = event.getMessage.getContentRaw
    val jda = event.getJDA
    val guild = event.getGuild

    // If there is no text (for example on file uploads) it's not a command
    if (message.isEmpty) {
      return
    }

    // We only react if the message begins with our reaction
    if (!message.startsWith(s"<@${jda.getSelfUser.getId}> ") &&
      !message.startsWith(s"<@!${jda.getSelfUser.getId}> ") &&
      !botRolePrefix(guild).exists(message.startsWith)) {
      return
    }

    val member = event.getMember
    val spaceSeparated = message.split(' ')
    val channel = event.getTextChannel

    // User tried eval
    if (evaluatedUsers.contains(member.getUser.getIdLong)) {
      return
    }

    // There is no command
    if (spaceSeparated.size < 2) {
      return
    }

    val commandAndArgs = spaceSeparated.slice(1, spaceSeparated.size)

    if (commandAndArgs.size > Byte.MaxValue) {
      channel
        .sendMessage(
          s"Maximum argument limit of ${Byte.MaxValue} " +
            s"reached (${commandAndArgs.size} arguments provided)"
        )
        .queue()
      return
    }

    val command = commandAndArgs.head.toLowerCase

    val manOption = commands.find(_.aliases.contains(command))

    if (manOption.isEmpty) {
      return
    }

    val man = manOption.get

    // It is terrifying how much people trust in bots - lets educate them
    // that they should not give bots too many permissions.
    val elevatedPermissions = unnededElevatedCommands(guild)
    if (elevatedPermissions.nonEmpty) {
      val memberPair = (member.getUser.getIdLong, guild.getIdLong)
      val exception = membersWhoKnowBetter.get(memberPair)

      if (exception.exists(System.currentTimeMillis() - _ >= 300000)) {
        membersWhoKnowBetter -= memberPair
      }

      if (exception.exists(System.currentTimeMillis() - _ < 300000)) {
        // The user has an exception, we update it
        membersWhoKnowBetter += ((memberPair, System.currentTimeMillis()))
      } else if (man == iKnowWhatIAmDoingCommand) {
        // The command is the command to bypass this check - ignore
      } else {
        // We should flip tables
        channel
          .sendMessage(
            "I have the following permissions which I do not need: " +
              elevatedPermissions
                .map({
                  case (permission, roles) =>
                    s"${permission.getName} (from ${roles
                      .map({ role =>
                        if (role.isMentionable || guild.getSelfMember.hasPermission(channel, MESSAGE_MENTION_EVERYONE))
                          s"``@${role.getName.replace("`", "\\`")}``" +
                          s"(ID: ${role.getId})"
                        else role.getAsMention
                      })
                      .mkString(", ")})"
                })
                .mkString(", ") + ".\n\n" +

              """This is a security issue since it gives anyone who compromises
                |this bot these permissions which could result in your server being
                |destroyed. We do the best to keep our bots safe, but mistakes still
                |happen.
            """.stripMargin.split('\n').mkString(" ").replace("  +", " ")
              + "\n\n" +

              """To keep using this bot, you must remove these permissions. If you
                |need additional guidance, ask at https://discord.gg\9prGCXT.
            """.stripMargin.split('\n').mkString(" ").replace("  +", " ")
          )
          .queue()

        return
      }
    }

    val missingBotPermissions =
      checkPermissions(guild.getSelfMember, channel, man.botPermissions)
    val missingUserPermissions =
      checkPermissions(member, channel, man.userPermissions)

    if (missingBotPermissions.nonEmpty || missingUserPermissions.nonEmpty) {
      if (!guild.getSelfMember.hasPermission(channel, MESSAGE_WRITE)) {
        // Now how are we going to deal with THAT?
        return
      }

      val message = (if (missingUserPermissions.isEmpty) ""
                     else {
                       "You are missing the following permissions: " +
                       missingUserPermissions.map(_.getName).mkString(", ")
                     }) +
        (if (missingBotPermissions.isEmpty) ""
         else
           "\nThe bot is missing the following permissions: " +
           missingBotPermissions.map(_.getName).mkString(", "))

      channel.sendMessage(message).queue()
      return
    }

    try {
      man.commandHandler(channel, guild, member, commandAndArgs)
    } catch {
      case ex: Exception =>
        val memberPair = (member.getUser.getIdLong, guild.getIdLong)

        if (membersWhoKnowBetter.contains(memberPair)) {
          dump(channel, member, commandAndArgs.mkString(" "), ex)
        } else {
          channel
            .sendMessage(
              "Something went wrong! Please report this bug " +
                "to <https://blame.garantiertnicht.software/>"
            )
            .queue()
        }
    }
  }

  override def onReady(event: ReadyEvent): Unit = {
    val user = event.getJDA.getSelfUser
    event.getJDA.getPresence
      .setActivity(Activity.watching(s"@${user.getAsTag} help"))
  }

  def helpCommandHandler(
    channel: TextChannel,
    guild: Guild,
    member: Member,
    commandAndArgs: Array[String]
  ): Unit =
    if (commandAndArgs.size == 1) {
      simpleHelpCommand(guild.getJDA, channel)
    } else {
      helpCommandForCommand(
        commandAndArgs.slice(1, commandAndArgs.size).mkString(" ").toLowerCase,
        guild.getJDA,
        channel,
        member,
        guild
      )
    }

  def addRoleCommandHandler(
    channel: TextChannel,
    guild: Guild,
    member: Member,
    strings: Array[String]
  ): Unit = {
    if (strings.length == 1) {
      helpCommandForCommand(
        strings.head,
        channel.getJDA,
        channel,
        member,
        guild
      )

      return
    }

    val embedBuilder = new EmbedBuilder()

    val rolesAndEmotes = roleEmoteList(strings.slice(1, strings.length), guild)
    val roles = rolesAndEmotes._1
    val emotes = rolesAndEmotes._2
    val unknown = rolesAndEmotes._3

    val uselessRoles =
      if (roles.isEmpty || emotes.isEmpty) emotes
      else {
        emotes
          .map(_.getRoles.asScala)
          .reduce(_ intersect _) intersect roles
      }
    val usefulRoles =
      if (roles.isEmpty || emotes.isEmpty) roles
      else {
        roles diff uselessRoles
      }

    if (usefulRoles.nonEmpty && emotes.nonEmpty) {
      emotes.foreach(
        emote =>
          emote.getManager
            .setRoles((emote.getRoles.asScala ++ usefulRoles).toSet.asJava)
            .queue(
              _ => {},
              (error: Throwable) => {
                channel
                  .sendMessage(
                    s"Updating ${emote.getAsMention} failed: ${error.getMessage}. " +
                      "This means that changes on thus emoji were **not** saved. Try " +
                      "again later. If the issue persists, please contact the developer."
                  )
                  .queue()
              }
            )
      )
      embedBuilder.setTitle("Success")
      embedBuilder.setDescription(
        "Your changes have been sent of to Discord. These are your new allowed " +
          "roles:\n" +

          emotes
            .map(
              emote =>
                s"${emote.getAsMention}: " +
                  (emote.getRoles.asScala ++ usefulRoles).distinct
                    .map(_.getAsMention)
                    .mkString(", ")
            )
            .mkString("\n")
      )

      if (unknown.isEmpty && uselessRoles.isEmpty) {
        embedBuilder.setColor(0x34e468)

        val restrictedEmotes =
          for {
            emote <- guild.getEmotes.asScala
            roles = emote.getRoles.size()
            if roles > 1
          } yield roles

        val totalRoleRestrictions = restrictedEmotes.sum

        if (totalRoleRestrictions >= 5) {
          embedBuilder.appendDescription(
            "\n\n[Leave a review for this bot!](https://bots.ondiscord.xyz/bots/553619116775309332/review)"
          )
        }
      } else {
        embedBuilder.setColor(0xe4d134)
      }

    } else {
      embedBuilder.setTitle("Failure")
      embedBuilder.setDescription(
        "You need to provide at least one role and " +
          "one emote."
      )
      embedBuilder.setColor(0xe43434)
    }

    if (unknown.nonEmpty) {
      embedBuilder.addField(
        "The following arguments where not understood and thus ignored:",
        unknown
          .mkString("\n") + s"\n\nPlease check ${helpCommand.canonicalName}" +
          s" ${addRoleCommand.canonicalName} for the correct syntax.",
        false
      )
    }

    if (uselessRoles.nonEmpty && emotes.nonEmpty) {
      embedBuilder.addField(
        s"The following roles where already " +
          s"${addRoleCommand.canonicalName}ed to all of those emojis and " +
          s"thus ignored:",
        uselessRoles.map(_.getAsMention).mkString(", ") +
          (if (usefulRoles.isEmpty)
             s"\nMaybe you want to try `${removeRoleCommand.canonicalName}` instead?"
           else ""),
        false
      )
    }

    channel.sendMessage(embedBuilder.build()).queue()
  }

  def removeRoleCommandHandler(
    channel: TextChannel,
    guild: Guild,
    member: Member,
    strings: Array[String]
  ): Unit = {
    val selfRole = selfManagedRole(guild)

    if (strings.length == 1) {
      helpCommandForCommand(
        strings.head,
        channel.getJDA,
        channel,
        member,
        guild
      )

      return
    }

    val embedBuilder = new EmbedBuilder()

    val rolesAndEmotes = roleEmoteList(strings.slice(1, strings.length), guild)
    val roles = rolesAndEmotes._1
    val emotes = rolesAndEmotes._2
    val unknown = rolesAndEmotes._3

    val uselessRoles =
      if (roles.isEmpty || emotes.isEmpty) roles
      else {
        val commonRoles = emotes
          .map(_.getRoles.asScala)
          .reduce(_ ++ _)

        roles diff commonRoles
      }
    val usefulRoles =
      if (roles.isEmpty || emotes.isEmpty) roles.toSet
      else {
        (roles diff uselessRoles).toSet
      }

    if (usefulRoles.nonEmpty && emotes.nonEmpty) {
      emotes.foreach { emote =>
        var roles: Set[Role] = emote.getRoles.asScala .toSet -- usefulRoles

        if (roles.size == 1 && selfRole.isDefined && roles.contains(selfRole.get)) {
          roles = Set()
        }

        emote.getManager
          .setRoles(roles.toSet.asJava)
          .queue(
            _ => {},
            (error: Throwable) => {
              channel
                .sendMessage(
                  s"Updating ${emote.getAsMention} failed: ${error.getMessage}. " +
                    "This means that changes on these emoji were **not** saved. Try " +
                    "again later. If the issue persists, please contact the developer."
                )
                .queue()
            }
          )
      }
      embedBuilder.setTitle("Success")
      embedBuilder.setDescription(
        "Your changes have been sent of to Discord. These are your new allowed " +
          "roles:\n" +

          emotes
            .map(emote => {
              val roles = emote.getRoles.asScala.toSet -- usefulRoles

              val useable =
                if (roles.isEmpty || (roles.size == 1 && selfRole.isDefined && roles.contains(selfRole.get)))
                  "@everyone"
                else
                  roles.map(_.getAsMention).mkString(", ")
              s"${emote.getAsMention}: $useable"
            })
            .mkString("\n")
      )

      if (unknown.isEmpty && uselessRoles.isEmpty) {
        embedBuilder.setColor(0x34e468)
      } else {
        embedBuilder.setColor(0xe4d134)
      }
    } else {
      embedBuilder.setTitle("Failure")
      embedBuilder.setDescription(
        "You need to provide at least one role and " +
          "one emote."
      )
      embedBuilder.setColor(0xe43434)
    }

    if (unknown.nonEmpty) {
      embedBuilder.addField(
        "The following arguments where not understood and thus ignored:",
        unknown
          .mkString("\n") + s"\n\nPlease check ${helpCommand.canonicalName}" +
          s" ${removeRoleCommand.canonicalName} for the correct syntax.",
        false
      )
    }

    if (emotes.nonEmpty && uselessRoles.nonEmpty) {
      embedBuilder.addField(
        s"The following roles are not ${addRoleCommand.canonicalName}ed " +
          s"to any of these emoji and were thus ignored:",
        uselessRoles.map(_.getAsMention).mkString(", ") +
          (if (usefulRoles.isEmpty)
             s"\nMaybe you want to try `${addRoleCommand.canonicalName}` instead?"
           else ""),
        false
      )
    }

    channel.sendMessage(embedBuilder.build()).queue()
  }

  def getRolesCommandHandler(
    channel: TextChannel,
    guild: Guild,
    member: Member,
    strings: Array[String]
  ): Unit = {
    if (strings.length == 1) {
      helpCommandForCommand(strings(0), channel.getJDA, channel, member, guild)
      return
    }

    val result = getRoleOrEmote(strings.slice(1, strings.length), guild)

    if (result._1.isEmpty || result._1.get.isLeft) {
      channel.sendMessage("Could not find that emote.").queue()
      return
    }

    val emote = result._1.get.right.get
    val roles = emote.getRoles.asScala

    if (roles.isEmpty) {
      channel.sendMessage("This emote is not restricted… yet ;)").queue()
      return
    }

    val embedBuilder = new EmbedBuilder()
    embedBuilder.setTitle(
      s"Roles ${addRoleCommand.canonicalName}ed for ${emote.getName}"
    )
    embedBuilder.setDescription(roles.map(_.getAsMention).mkString(" "))

    val selfRole = selfManagedRole(guild)
    if (selfRole.isDefined && roles.contains(selfRole.get))
      embedBuilder.setFooter(
        s"The ${selfRole.get.getName} role is toggled automagically as a " +
          "workaround of a Discord Bug. You don't need to specify it when " +
          "making this emoji public again."
      )

    channel.sendMessage(embedBuilder.build()).queue()
  }

  def evaluateUserCommandHandler(
    channel: TextChannel,
    guild: Guild,
    member: Member,
    strings: Array[String]
  ) = {
    evaluatedUsers += member.getUser.getIdLong
    val embedBuilder = new EmbedBuilder
    embedBuilder.setTitle("User evaluation ended with a FREE upgrade!")
    embedBuilder.setDescription(
      """Upon a quick overview over your recent commands, the space alien
        |decided that you are eligible for a **FREE** Not-A-Wrecking-Ball®
        |from World Domination Inc. With the World Domination Inc.
        |Not-A-Wrecking-Ball® you can efficiently handle all violations of
        |your planned policy _yourself_! Note that this is an upgrade to the
        |existing Emoji Restrictor experience and thus the normal commands
        |are no longer available to you.
        |
        |For a quick overview on how to efficiently use the World Domination
        |Inc. Not-A-Wrecking-Ball® please see our introductionary video:
        |<https://youtu.be/FXPKJUE86d0>. If you for some bizarre reason prefer
        |the old experience you may apply for a refund at
        |<https://discord.gg/Ew3wyJj>.
      """.stripMargin
    )

    channel.sendMessage(embedBuilder.build).queue()

    channel.getJDA
      .getTextChannelById(396300705683472386L)
      .sendMessage(
        s"${member.getAsMention} executed eval in " +
          s"${guild.getIdLong}/<#${channel.getId}> [@everyone]"
      )
      .queue()
  }

  def iKnowWhatIAmDoingCommandHandler(
    channel: TextChannel,
    guild: Guild,
    member: Member,
    commandAndArgs: Array[String]
  ): Unit = {
    membersWhoKnowBetter +=
      ((member.getUser.getIdLong, guild.getIdLong) -> System
        .currentTimeMillis())
    channel
      .sendMessage("You now bypass this security check temporarily.")
      .queue()
  }

  def simpleHelpCommand(jda: JDA, channel: TextChannel): Unit = {
    val commands =
      Seq(helpCommand, addRoleCommand, removeRoleCommand, getRolesCommand).map(
        command => {
          (
            command.aliases.head,
            command.syntax
              .map(syntax => s"${jda.getSelfUser.getAsMention} $syntax")
              .mkString("\n") + "\n" + command.overview
          )
        }
      )

    val about = (
      "About",
      s"""Discord has the ability to restrict custom emojis to a list or roles,
         |but does not offer any way to set it though the client. This bot
         |offers ${jda.getGuilds.size} servers just that.
       """.stripMargin.linesIterator.mkString(" ") + "\n\n" +
        s"""In order to start restricting a role, `${addRoleCommand.canonicalName}`
           |each of the roles the emoji you like to restrict. If you feel like it,
           |you can `${removeRoleCommand.canonicalName}` roles from using an emoji
           |again. If all roles are `${removeRoleCommand.canonicalName}`d, it is
           |public again.
       """.stripMargin.linesIterator.mkString(" ")
    )

    val randomLink = Seq(
      "[Looking for Piano Lessons?](https://youtu.be/kIvobmqIJu8)",
      "[Hating people smiling all the time?](https://youtu.be/dP9Wp6QVbsk)",
      "[🔥 This Bot is on Fire! 🔥](https://youtu.be/yad0Qa1TSKQ)",
      "[Unicorns!](https://www.youtube.com/watch?v=epRapFSHFFM)"
    )(Random.nextInt(4))
    val links = (
      "Links",
      s"""[Source Code (licensed under AGPL3 or later)](https://gitlab.com/garantiertnicht/emoji-restrictor)
         |[Invite Link](https://discordapp.com/api/oauth2/authorize?client_id=553619116775309332&permissions=1074023425&redirect_uri=https%3A%2F%2Fdiscord.gg%2F9prGCXT&response_type=code&scope=bot)
         |[Support Server](https://discord.gg/9prGCXT)
         |[Privacy Policy & Terms](https://discord.gg/BhxxJTetPR)
         |$randomLink
       """.stripMargin
    )

    val embedBuilder = new EmbedBuilder()
    embedBuilder.setTitle(about._1)
    embedBuilder.setDescription(about._2)

    commands.foreach(
      command => embedBuilder.addField(command._1, command._2, false)
    )

    embedBuilder.addField(links._1, links._2, false)

    channel.sendMessage(embedBuilder.build()).queue()
  }

  def helpCommandForCommand(
    commandName: String,
    jda: JDA,
    channel: TextChannel,
    member: Member,
    guild: Guild
  ): Unit = {
    val embedBuilder = new EmbedBuilder()
    val command = commands.find(_.aliases.contains(commandName.toLowerCase))

    if (command.isEmpty) {
      embedBuilder.setTitle("No such command")
      embedBuilder.setDescription(
        "Use help without a command for a list of commands"
      )
      embedBuilder.build()
    } else {
      val man = command.get
      embedBuilder.setTitle(s"Manual for ${man.aliases.head}")
      embedBuilder.setDescription(
        man.aliases.mkString(", ") + "\n\n" + man.overview
      )
      embedBuilder.addField(
        "Sypnosis",
        man.syntax
          .map(syntax => s"${jda.getSelfUser.getAsMention} $syntax")
          .mkString("\n"),
        false
      )

      embedBuilder.addField("Description", man.description, false)

      embedBuilder.addField(
        "Parameters",
        man.parameters
          .map(pair => {
            val name = pair._1
            val description = pair._2

            s"**$name** $description"
          })
          .mkString("\n"),
        false
      )

      val emote = Random
        .shuffle(guild.getEmotes.asScala)
        .find(emote => guild.getSelfMember.canInteract(emote))
        .getOrElse(jda.getEmoteById(406799287503159296L))
      val role = guild.getRoles.get(Random.nextInt(guild.getRoles.size()))

      embedBuilder.addField(
        "Examples",
        man.examples
          .map(example => {
            s"${jda.getSelfUser.getAsMention} " + example
              .replaceAll("\\{emoteId\\}", emote.getId)
              .replaceAll("\\{emoteName\\}", s"${emote.getName}")
              .replaceAll("\\{emoteMention\\}", emote.getAsMention)
              .replaceAll("\\{roleId\\}", role.getId)
              .replaceAll("\\{roleName\\}", role.getName)
              .replaceAll("\\{roleMention\\}", role.getAsMention)
          })
          .mkString("\n"),
        false
      )

      if (man.botPermissions.nonEmpty) {
        embedBuilder.addField(
          "Required Bot Permissions",
          man.botPermissions
            .map(permission => {
              val name = permission.getName

              if (checkPermission(guild.getSelfMember, channel, permission)) {
                name
              } else {
                s"**$name**"
              }
            })
            .mkString(", "),
          false
        )
      }

      if (man.userPermissions.nonEmpty) {
        embedBuilder.addField(
          "Required User Permissions",
          man.userPermissions
            .map(permission => {
              val name = permission.getName

              if (checkPermission(member, channel, permission)) {
                name
              } else {
                s"**$name**"
              }
            })
            .mkString(", "),
          false
        )
      }
    }

    channel.sendMessage(embedBuilder.build()).queue()
  }

  def roleEmoteList(
    string: Seq[String],
    guild: Guild,
    roles: Seq[Role] = Seq(),
    emotes: Seq[Emote] = Seq(),
    unknown: Seq[String] = Seq()
  ): (Seq[Role], Seq[Emote], Seq[String]) = {
    val result = getRoleOrEmote(string, guild)
    val consumed = result._2
    if (result._1.nonEmpty) {
      val emoteOrEmote = result._1.get

      if (emoteOrEmote.isLeft) {
        if (string.size - consumed <= 0) {
          (emoteOrEmote.left.get +: roles, emotes, unknown)
        } else {
          roleEmoteList(
            string.slice(consumed, string.size),
            guild,
            emoteOrEmote.left.get +: roles,
            emotes,
            unknown
          )
        }
      } else {
        if (string.size - consumed <= 0) {
          (roles, emoteOrEmote.right.get +: emotes, unknown)
        } else {
          roleEmoteList(
            string.slice(consumed, string.size),
            guild,
            roles,
            emoteOrEmote.right.get +: emotes,
            unknown
          )
        }
      }
    } else {
      val unconsumed = string.slice(0, consumed).mkString(" ")
      if (string.size - consumed <= 0) {
        (roles, emotes, unconsumed +: unknown)
      } else {
        roleEmoteList(
          string.slice(consumed, string.size),
          guild,
          roles,
          emotes,
          unconsumed +: unknown
        )
      }
    }
  }

  def getRoleOrEmote(
    string: Seq[String],
    guild: Guild
  ): (Option[Either[Role, Emote]], Byte) = {
    if (string.head.nonEmpty && !string.head.exists(char => !char.isDigit)) {
      // It is probably an ID
      val role = guild.getRoleById(string.head)
      val emote = guild.getEmoteById(string.head)

      return if (role != null && emote != null) {
        // Pretty unlikely anyways
        (None, 1)
      } else if (role != null) {
        (Some(Left(role)), 1)
      } else if (emote != null) {
        (Some(Right(emote)), 1)
      } else {
        (None, 1)
      }
    }

    if (string.head.startsWith("<@&")) {
      val id = string.head.substring(3, string.head.length - 1)

      if (id.isEmpty) {
        return (None, 1)
      }

      val role = guild.getRoleById(id)

      if (role == null) {
        return (None, 1)
      }

      return (Some(Left(role)), 1.toByte)
    }

    if (string.head.startsWith("<:") || string.head.startsWith("<a:")) {
      val parts = string.head.split(":")
      val id = parts.last.substring(0, parts.last.length - 1)

      if (id.isEmpty) {
        return (None, 1)
      }

      val emote = guild.getEmoteById(id)

      if (emote == null) {
        return (None, 1)
      }

      return (Some(Right(emote)), 1)
    }

    def getByName[A](
      quoteCharacter: String,
      lookupFunction: (String, Boolean) => util.List[A]
    ): Option[(Option[A], Byte)] = {
      if (string.head.startsWith(quoteCharacter)) {
        val index = string.indexWhere(_.endsWith(quoteCharacter))
        if (index >= 0) {
          val nameWithQuotes = string.slice(0, index + 1).mkString(" ")
          val name = nameWithQuotes.substring(1, nameWithQuotes.length - 1)
          val entities: util.List[A] = lookupFunction(name, true)

          if (entities.size == 1) {
            return Some((Some(entities.get(0)), (index + 1).toByte))
          } else {
            return Some(None, (index + 1).toByte)
          }
        }
      }

      None
    }

    val roleByString = getByName("`", guild.getRolesByName)
    val emoteByString = getByName(":", guild.getEmotesByName)

    if (roleByString.nonEmpty) {
      val role = roleByString.get
      return (role._1.map(role => Left(role)), role._2)
    }

    if (emoteByString.nonEmpty) {
      val emote = emoteByString.get
      return (emote._1.map(emote => Right(emote)), emote._2)
    }
    (None, 1)
  }

  def unnededElevatedCommands(guild: Guild): Map[Permission, Set[Role]] = {
    val member = guild.getSelfMember
    val requiredPermissions = commands.flatMap(_.botPermissions).toSet ++ Set(
      CREATE_INSTANT_INVITE
    )

    val elevatedPermissions = Set(
      ADMINISTRATOR,
      BAN_MEMBERS,
      KICK_MEMBERS,
      MANAGE_CHANNEL,
      MANAGE_EMOTES,
      MANAGE_PERMISSIONS,
      MANAGE_SERVER,
      MANAGE_WEBHOOKS,
      MESSAGE_MANAGE,
      NICKNAME_MANAGE,
      MESSAGE_MENTION_EVERYONE
    ) diff requiredPermissions

    guild.getSelfMember.getRoles.asScala.toSet
      .flatMap((role: Role) => {
        (role.getPermissions.asScala.toSet intersect elevatedPermissions)
          .map(_ -> role)
      })
      .groupBy(_._1)
      .map({
        case (permission, roles) =>
          permission -> roles.map(_._2)
      })
  }

  def checkPermissions(
    who: IPermissionHolder,
    where: TextChannel,
    which: Seq[Permission]
  ): Seq[Permission] =
    which.filterNot(checkPermission(who, where, _))

  def checkPermission(
    who: IPermissionHolder,
    where: TextChannel,
    which: Permission
  ) =
    if (which.isChannel) who.hasPermission(where, which)
    else who.hasPermission(which)

  def dump(
    channel: TextChannel,
    member: Member,
    command: String,
    exception: Exception
  ): Unit = {
    val guild = channel.getGuild
    val bot = guild.getSelfMember

    if (!bot.hasPermission(channel, MESSAGE_ATTACH_FILES)) {
      channel
        .sendMessage(
          "**You discovered a happy little feature!**" +
            "To get an exception dump, please enable the Attach Files " +
            "permission in this channel."
        )
        .queue()
    }

    val ex = new StringWriter()
    val exPrint = new PrintWriter(ex)
    exception.printStackTrace(exPrint)
    exPrint.close()
    val exceptionText = ex.getBuffer.toString
    ex.close()

    val string =
      s"""Bot: ${bot.getUser.getId}
         |Time: ${Instant.now().toString}
         |User: ${member.getUser.getId}
         |Guild: ${guild.getId}
         |Channel: ${channel.getId}
         |Bot Permissions Guild: ${bot
           .asInstanceOf[IPermissionHolder]
           .getPermissions
           .asScala
           .mkString(" ")}
         |Bot Permissions Channel: ${bot
           .getPermissions(channel)
           .toArray
           .mkString(" ")}
         |User Permissions Guild: ${member
           .asInstanceOf[IPermissionHolder]
           .getPermissions()
           .asScala
           .mkString(" ")}
         |User Permissions Channel: ${member
           .getPermissions(channel)
           .toArray
           .mkString(" ")}
         |
         |--COMMAND--
         |$command
         |
         |--EXCEPTION--
         |$exceptionText
         |""".stripMargin

    channel
      .sendMessage(
        "**You found a happy little feature!** " +
          "Please attach this exception dump in your report to " +
          "<https://blame.garantiertnicht.software/>"
      )
      .addFile(string.getBytes, "exception_dump.txt")
      .queue()
  }

  def botRolePrefix(guild: Guild): Option[String] = {
    val roles =
      for {
        role <- guild.getSelfMember.getRoles.asScala
        if role.isManaged && guild.getMembersWithRoles(role).size() == 1
      } yield role

    roles.headOption.map(_.getAsMention + " ")
  }

  /**
   * There's a very old bug in Discord: Roles getting a added to a member doesn't invalidate the client side cache of
   * usable emojis. This method works around by updating an emote.
   *
   * @param rolesAdded   The roles being added to a member
   * @param rolesPresent The roles the member already has
   * @todo Remove this once Discord fixes this
   */
  def roleAddedWorkaround(rolesAdded: Set[Role], rolesPresent: Set[Role]): Unit = {
    if (rolesAdded.isEmpty) return
    val guild = rolesAdded.head.getGuild

    // Get an emote we need to add
    val emoteOption = guild.getEmotes.asScala.find { emote =>
      val roles = emote.getRoles.asScala

      roles.exists(rolesAdded.contains) && !roles.exists(rolesPresent.contains)
    }

    if (emoteOption.isEmpty) return
    val emote = emoteOption.get

    // Find the managed role
    val roleOption = selfManagedRole(guild)
    if (roleOption.isEmpty) return
    val role = roleOption.get

    var newRoles = emote.getRoles.asScala.toSet

    // Toggle the managed role of the bot to cause an update - this will cause clients to invalidate their cache
    if (newRoles.contains(role))
      newRoles -= role
    else
      newRoles += role

    // Send it to Discord
    emote.getManager.setRoles(newRoles.asJava).queue()
  }

  override def onGuildMemberRoleAdd(event: GuildMemberRoleAddEvent): Unit = {
    roleAddedWorkaround(
      event.getRoles.asScala.toSet,
      event.getMember.getRoles.asScala.toSet -- event.getRoles.asScala
    )
  }

  def selfManagedRole(guild: Guild) =
    guild.getSelfMember.getRoles.asScala.find(_.isManaged)
}
